import { render } from 'react-dom';

import Root from './Root';

import styles from './styles/global.scss';

const mountPoint = document.createElement('div');
mountPoint.className = styles.root;
document.body.appendChild(mountPoint);

render(Root, mountPoint);