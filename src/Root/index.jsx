import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

import AuthForm from './AuthForm';

const Root = React.memo(() => {
   return <Router>
       <Switch>
           <Route path={ '/auth' } component={AuthForm}/>
           <Redirect to={ '/auth' }/>
       </Switch>
   </Router>;
});

export default <Root/>;