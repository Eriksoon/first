import propTypes from 'prop-types';
import React from 'react';
import cn from 'classnames';

import styles from './styles.scss';

const AuthForm = (props) => {
    const {} = props;

    return <div className={styles.root}>
        <h1 className={styles.heading}>AuthForm</h1>
    </div>;
};

AuthForm.propTypes = {};

AuthForm.defaultProps = {};

export default AuthForm;