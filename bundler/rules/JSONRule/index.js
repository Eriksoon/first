export default ({ isProduction }) => ({
  test: /\.json/,
  loader: "json-loader",
  query: {
    limit: 1024 * 5,
    name: `static/other/[name]${isProduction ? '.[hash]' : ''}.[ext]`,
  },
})