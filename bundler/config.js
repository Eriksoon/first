const port = '9004';
const host = '127.0.0.1';
const srcDir = 'src';
const distDir = 'public';
const faviconsDir = 'favicons';
const shareDir = 'share';

export default {
  port,
  host,
  srcDir,
  distDir,
  faviconsDir,
  shareDir,
}