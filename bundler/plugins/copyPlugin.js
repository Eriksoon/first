import path from 'path';

import CopyWebpackPlugin from 'copy-webpack-plugin';


export default ({ srcPath, outputPath, faviconsDir, shareDir }) => [
  new CopyWebpackPlugin([
    {
      from: path.resolve(srcPath, faviconsDir),
      to: path.resolve(outputPath, faviconsDir),
      toType: 'dir'
    }
  ]),

  new CopyWebpackPlugin([
    {
      from: path.resolve(srcPath, shareDir),
      to: path.resolve(outputPath, shareDir),
      toType: 'dir'
    }
  ]),
];
