import path from 'path';

import HtmlWebpackPlugin from 'html-webpack-plugin';


export default ({ srcPath, outputPath, isProduction }) => new HtmlWebpackPlugin({
  filename: path.resolve(outputPath, 'index.html'),
  template: path.resolve(srcPath, 'index.html' ),
  inject: true,
  isProduction
});